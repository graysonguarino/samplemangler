//
//  PlayerView.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/7/21.
//

import SwiftUI
import CoreMotion
import AudioKit
import AVFoundation
import Combine
import DunneAudioKit


class PlayerConductor: ObservableObject {
    
    @Published var x: Double = 0 // left and right
    @Published var y: Double = 0 // up and down
    
    @Published var initialX: Double = 0 // Could these be lets?
    @Published var initialY: Double = 0
    
    
    private var tracks: [MIDINoteData] = [
        MIDINoteData(
            noteNumber: MIDINoteNumber(24),
            velocity: MIDIVelocity(127),
            channel: MIDIChannel(0),
            duration: Duration(beats: 16),
            position: Duration(beats: 0)
            ),
        MIDINoteData(
            noteNumber: MIDINoteNumber(24),
            velocity: MIDIVelocity(127),
            channel: MIDIChannel(0),
            duration: Duration(beats: 16),
            position: Duration(beats: 0)
        ),
        MIDINoteData(
            noteNumber: MIDINoteNumber(24),
            velocity: MIDIVelocity(127),
            channel: MIDIChannel(0),
            duration: Duration(beats: 16),
            position: Duration(beats: 0)
        ),
        MIDINoteData(
            noteNumber: MIDINoteNumber(24),
            velocity: MIDIVelocity(127),
            channel: MIDIChannel(0),
            duration: Duration(beats: 16),
            position: Duration(beats: 0)
        )
    ]
    
    
    @Published var shouldPlaySample: [Bool] = [false, false, false, false] {
        didSet {
            for (i, track) in tracks.enumerated() {
                if shouldPlaySample[i] {
                    sequencer.tracks[i].add(midiNoteData: track)
                } else {
                    sequencer.tracks[i].clear()
                }
            }
        }
    }
    
    
    let motionManager = CMMotionManager()
    
    @Published private(set) var lastPlayed: String = "None"
    
    // I have to do initialize with a MIDI file and ERASE THE DATA. AppleSequencer is borked.
    let sequencer = AppleSequencer(fromURL: (Bundle.main.resourceURL?.appendingPathComponent("4tracks.midi"))!)
    let engine = AudioEngine()
    let mixer = Mixer(volume: 1.0, name: "Main Mixer")
    
    let samplers = [MIDISampler(name: "Sampler 1"), MIDISampler(name: "Sampler 2"), MIDISampler(name: "Sampler 3"), MIDISampler(name: "Sampler 4")]
    
    var reverb: Reverb
    var delay: Delay
    var limiter: PeakLimiter
    
    init() {
        sequencer.clearRange(start: Duration(beats: 0), duration: Duration(beats: 100))
        sequencer.debug()
        sequencer.enableLooping(Duration(beats: 16))
        sequencer.setTempo(BPM(138))

        
        for (i, sampler) in samplers.enumerated() {
            sampler.enableMIDI()
            sequencer.tracks[i].setMIDIOutput(sampler.midiIn)
            mixer.addInput(sampler)
        }
        mixer.start()
        reverb = Reverb(mixer)
        reverb.loadFactoryPreset(AVAudioUnitReverbPreset.smallRoom)
        reverb.start()
        delay = Delay(reverb)
        delay.start()
        limiter = PeakLimiter(delay)
        limiter.start()
    }
    
    
    @Published var tempo: Float = 138 {
        didSet {
            sequencer.setTempo(BPM(tempo))
        }
    }
    
    @Published var isPlaying = false {
        didSet {
            isPlaying ? sequencer.play() : sequencer.stop()
        }
    }

    
    func getGyroState() {
        motionManager.deviceMotionUpdateInterval = 0.0083 // Updates around 120 times per second for ProMotion smoothness
        
        let queue = OperationQueue()
        motionManager.startDeviceMotionUpdates(to: queue, withHandler: { [weak self] motion, error in
            if let attitude = motion?.attitude {
                DispatchQueue.main.async {
                    if self?.initialX == 0 { // Calibration
                        self?.initialX = attitude.yaw
                        self?.initialY = attitude.roll
                    }
                    
                    self?.x = attitude.yaw
                    self?.y = attitude.roll
                }
            }
        })
    }
    
    
    func updateSamplers(track: Track) {
        do {
            let files = track.trackSamples.map { $0.audioFile }
            for i in 0...3 {
                try samplers[i].loadAudioFile(files[i]!)
            }
        } catch { Log("Samples didn't load!") }
    }
    
    func updateTrack(track: Track) {
        // Want to change tempo to repitch samples, but MIDISampler doesn't support repitching.
        // Sampler does, but it doesn't have MIDI input.
        tempo = Float(track.tempo)
        reverb.dryWetMix = AUValue(track.reverbMix)
        delay.time = AUValue(track.delayTime)

    }
    
    func updateFX(track: Track) {
        delay.feedback = AUValue(track.delayFeedback)
        delay.dryWetMix = AUValue(track.delayMix)
    }
    
    
    func start() {
        engine.output = limiter
        
        do { try engine.start() }
        catch { Log("AudioKit did not start: \(error)") }
    }
        
        
    func stop() {
        engine.stop()
        mixer.stop()
    }
}

struct PlayerView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @StateObject var track: Track
    @StateObject var conductor = PlayerConductor()
    
    @State var whichSample: Int = 0
    @State var shouldOpenSampleEditView = false
    @State var shouldOpenTrackEditView = false
    
    init(track: Track) {
        self._track = StateObject(wrappedValue: track)
    }
    

    private func getCoordinates() -> CGPoint {
        let centerX = UIScreen.main.bounds.maxX / 2
        let centerY = UIScreen.main.bounds.maxY / 2
        // adjustedX/Y - Ratio of distance away from center
        let adjustedX = (self.conductor.x - self.conductor.initialX) + 1
        let adjustedY = (self.conductor.y - self.conductor.initialY) + 1
        
        func convertDelayValue(_ val: Double) -> Double {
            // halves feedback/mix, ensures in [0, 100]
            var adjVal = (val / 2) * 100
            adjVal = adjVal >= 0 ? adjVal : 0 // Ensures greater than or equal to 0
            adjVal = adjVal <= 100 ? adjVal : 100 // Ensures less than or equal to 100
            return adjVal
        }
        
        track.delayFeedback = convertDelayValue(adjustedX)
        track.delayMix = abs(100 - convertDelayValue(adjustedY))
        conductor.updateFX(track: track)
        
        return CGPoint(x: centerX * adjustedX, y: centerY * adjustedY)
    }
    
    var body: some View {
        ZStack {
            VStack {
                Toggle("Play Sequence", isOn: $conductor.isPlaying)
                    .simultaneousGesture(
                        LongPressGesture()
                            .onEnded { _ in shouldOpenTrackEditView = true }
                    ) // Change tempo
                    .toggleStyle(.button)
                    .tint(.green)
                
                Toggle("Sample 1", isOn: $conductor.shouldPlaySample[0])
                    .simultaneousGesture(
                        LongPressGesture()
                            .onEnded { _ in
                                shouldOpenSampleEditView = true
                                whichSample = 0
                            }
                    )  // Change sample/FX
                    .toggleStyle(.button)
                    .tint(.purple)
                
                Toggle("Sample 2", isOn: $conductor.shouldPlaySample[1])
                    .simultaneousGesture(
                        LongPressGesture()
                            .onEnded { _ in
                                shouldOpenSampleEditView = true
                                whichSample = 1
                            }
                    )
                    .toggleStyle(.button)
                    .tint(.purple)
                
                Toggle("Sample 3", isOn: $conductor.shouldPlaySample[2])
                    .simultaneousGesture(
                        LongPressGesture()
                            .onEnded { _ in
                                shouldOpenSampleEditView = true
                                whichSample = 2
                            }
                    )
                    .toggleStyle(.button)
                    .tint(.purple)
                
                Toggle("Sample 4", isOn: $conductor.shouldPlaySample[3])
                    .simultaneousGesture(
                        LongPressGesture()
                            .onEnded { _ in
                                shouldOpenSampleEditView = true
                                whichSample = 3
                            }
                    )
                    .toggleStyle(.button)
                    .tint(.purple)
            }
                
            Image(systemName: "circle")
                .position(getCoordinates())
            
        }
        .onAppear {
            self.conductor.getGyroState()
            self.conductor.start()
            self.conductor.updateSamplers(track: track)
            self.conductor.updateTrack(track: track)
        }
        .onDisappear(perform: self.conductor.stop)
        .onDisappear {
            try! viewContext.save()
        }
        .background (
            // This background thing gets confusing. Basically, I have a hidden background that listens for $shouldOpenTrackEditView
            // and $shouldOpenSampleEditView which gets set to True upon long press of relevant buttons
            NavigationLink(destination: TrackEditView().environmentObject(track), isActive: $shouldOpenTrackEditView) {
                EmptyView()
            }.hidden()
                .onDisappear {
                    try! viewContext.save()
                    conductor.updateTrack(track: track)
                }
        )
        .background(
            NavigationLink(destination: SampleEditView(whichSample: whichSample).environmentObject(track), isActive: $shouldOpenSampleEditView) {
                EmptyView()
            }.hidden()
                .onDisappear { try! viewContext.save() }
        )
        .environmentObject(conductor)
    }
}

