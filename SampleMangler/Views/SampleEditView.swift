//
//  SampleEditView.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/12/21.
//

import SwiftUI

struct SampleEditView: View {
    @EnvironmentObject var track: Track
    @EnvironmentObject var conductor: PlayerConductor
    let whichSample: Int
    
    init(whichSample: Int) {
        self.whichSample = whichSample
    }
    
    var body: some View {
        Picker("Sample", selection: $track.trackSamples[whichSample]) {
            ForEach(0..<samples.count) { i in
                Text(samples[i].name).tag(samples[i])
            }
        }.onSubmit {
            conductor.updateSamplers(track: track)
        }
        Text("Sample \(whichSample + 1): \(track.trackSamples[whichSample].name)")
            .font(.headline)
            .foregroundColor(.green)
    }
}
