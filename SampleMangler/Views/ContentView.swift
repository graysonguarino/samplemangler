//
//  ContentView.swift
//  SampleMangler
//
//  Created by Anthony Grayson Guarino (C00281414) on 11/7/21.
//  Used to load in and "mangle" various samples with delay and reverb.
//  Implements gestures (08c), audio players via AudioKit (09b), animation (11a), and gyroscope (not in 8-11, but approved).
//
//  Add a new track on the home page and open it. You can select four samples to play at the same time,
//  and new samples can be chosen by long-pressing the Sample # button. The samples can be "mangled" by tilting
//  the phone. The x-axis controls delay feedback, the y-axis controls delay mix.
//  Track properties (name, reverb mix, delay time) can be modified by long-pressing Play Sequence.
//  Tracks and Samples are saved onDisappear().
//
//  Future ideas for this app include the ability to change the effects on the x and y axes and what parameter tilting controls.
//  A more consistent gyroscope calibration method would be nice as sometimes the tilting drifts or the
//  circle disappears. The circle gets recentered when the PlayerView is brought back into the main view
//  (can easily be done by opening the Sample or Track Editor and going back.
//  Value smoothing would be nice as well so that things like delay time can be affected without causing crackling.
//  Tempo changing would also be nice, but the current MIDISampler does not allow for repitching or time stretching.
//  Importing of new samples would be cool too.
//
//  This app makes heavy use of AudioKit [https://github.com/AudioKit/AudioKit]. All code graciously "inspired" by others
//  has been documented to the best of my ability. Much of this code does not use Swift to its full potential. This
//  is mostly due to having to spend most of my time wrapping my head around AudioKit and Core Data instead of the language.
//  There are inconsistencies in the way the app updates and listens for changes in values. Future renditions of this app
//  should make better use of Bindings and Environments, but these are concepts I am still new to, especially in Swift.
//  The panic coding is visible. I'm sure the GitLab is a hoot and a half to read through.
//  The repo is visible here. [https://gitlab.com/graysonguarino/samplemangler]
//
//  This should be treated as a demo more than a finished project. Have fun.

import SwiftUI
import CoreData

struct ContentView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Track.name, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Track>
    
    var body: some View {
        NavigationView {
            List {
                ForEach(items) { item in
                    NavigationLink {
                        PlayerView(track: item)
                    } label: {
                        Text(item.name)
                    }
                }
                .onDelete(perform: deleteItems)
            }
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    EditButton()
                }
                ToolbarItem {
                    Button(action: addItem) {
                        Label("Add Item", systemImage: "plus")
                    }
                }
            }
            Text("Select an item")
        }
    }

    private func addItem() {
        
        withAnimation {
            let newTrack = Track(context: viewContext)
            newTrack.name = Date.now.formatted()
            newTrack.id = UUID()
            newTrack.tempo = 138
            newTrack.reverbMix = 0.5
            newTrack.delayTime = 1.0

            for i in 0...3 {
                // Add default samples to track
                newTrack.trackSamples.append(samples[i])
            }
            
            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError.description), \(nsError.userInfo)")
            }
        }
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError.localizedDescription), \(nsError.userInfo)")
            }
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
