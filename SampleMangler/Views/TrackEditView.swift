//
//  TrackEditView.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/12/21.
//

import SwiftUI

struct TrackEditView: View {
    @EnvironmentObject var track: Track
    @EnvironmentObject var conductor: PlayerConductor
    
    // sliderConverter is unused currently, but may prove useful later.
    var sliderConverter: Binding<Double>{ // Converts slider's Double to tempo's expected Int64
            Binding<Double>(get: {
                return Double(track.tempo)
            }, set: {
                //rounds the double to an Int
                print($0.description)
                track.tempo = Int64($0)
            })
        }
    
    var body: some View {
        TextField("Track Name", text: $track.name).foregroundColor(.purple)
        // Removing tempo slider. See note in PlayerView.updateTrack()
//        Slider(value: sliderConverter, in: 60...180, step: 1)
        HStack {
            Text("Reverb Mix")
            Slider(value: $track.reverbMix, in: 0...1) {
                Text("Reverb Mix") // Where is the label? Why isn't this visible?
            } minimumValueLabel: {
                Text("0")
            } maximumValueLabel: {
                Text("100")
            }
        }
        
        HStack {
            Text("Delay Time")
            Slider(value: $track.delayTime, in: 0...5) {
                Text("Delay Time")
            } minimumValueLabel: {
                Text("0")
            } maximumValueLabel: {
                Text("5")
            }
        }
        

    }
}

