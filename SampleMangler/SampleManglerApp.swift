//
//  SampleManglerApp.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/7/21.
//

import SwiftUI

@main
struct SampleManglerApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
