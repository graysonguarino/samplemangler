//
//  Samples.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/29/21.
//
//


let samples: [Sample] = [
    Sample(prettyName: "Alien Transmission", file: "Samples/Alien Transmission_C1.wav", note: 24),
    Sample(prettyName: "Android Chamber", file: "Samples/Android Chamber_C1.wav", note: 24),
    Sample(prettyName: "Aquamarine Static", file: "Samples/Aquamarine Static_C1.wav", note: 24),
    Sample(prettyName: "Belly Button", file: "Samples/Belly Button_C1.wav", note: 24),
    Sample(prettyName: "Boys Choir", file: "Samples/Boys Choir_C1.wav", note: 24),
    Sample(prettyName: "Breath Control", file: "Samples/Breath Control_C1.wav", note: 24),
    Sample(prettyName: "Dial Up", file: "Samples/Dial Up_C1.wav", note: 24),
    Sample(prettyName: "Random Triples", file: "Samples/Random Triples_C1.wav", note: 24)
]
