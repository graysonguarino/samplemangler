//
//  SampleTransformer.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/29/21.
//  "Inspired" by https://danielbernal.co/coredata-transformable-and-nssecurecoding-in-ios-13/

import Foundation

@objc(SampleTransformer)
class SampleTransformer: NSSecureUnarchiveFromDataTransformer {
    
    override static var allowedTopLevelClasses: [AnyClass] {
        [NSArray.self, Sample.self]
    }
    
    static func register() {
        let className = String(describing: SampleTransformer.self)
        let name = NSValueTransformerName(className)
        let transformer = SampleTransformer()
        
        ValueTransformer.setValueTransformer(transformer, forName: name)
    }
}
