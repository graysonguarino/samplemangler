//
//  Track+CoreDataProperties.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/29/21.
//
//

import Foundation
import CoreData
import AVFoundation


extension Track {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Track> {
        return NSFetchRequest<Track>(entityName: "Track")
    }

    @NSManaged public var id: UUID
    @NSManaged public var name: String
    @NSManaged public var tempo: Int64
    @NSManaged public var trackSamples: [Sample]
    @NSManaged public var reverbMix: Double
    @NSManaged public var delayFeedback: Double
    @NSManaged public var delayTime: Double
    @NSManaged public var delayMix: Double

}


extension Track {

}

extension Track : Identifiable {

}
