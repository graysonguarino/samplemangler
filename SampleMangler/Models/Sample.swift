//
//  Sample.swift
//  SampleMangler
//
//  Created by Grayson Guarino on 11/27/21.
//

import Foundation
import AudioKit
import AVFoundation

@objc(Sample)
public class Sample: NSObject, NSSecureCoding, Identifiable {

    public static func == (lhs: Sample, rhs: Sample) -> Bool {
        return lhs.id == rhs.id
    }


    public var audioFile: AVAudioFile? // Just store URL. Write func to convert.
    public var id: UUID
    public var file: String
    public var name: String
    public var rootNote: Int

    public static var supportsSecureCoding = true

    public func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(name, forKey: "name")
        coder.encode(rootNote, forKey: "rootNote")
        coder.encode(file, forKey: "file")
    }

    public required init?(coder: NSCoder) {
        
        id = coder.decodeObject(of: NSUUID.self, forKey: "id")! as UUID
        file = coder.decodeObject(of: NSString.self, forKey: "file")! as String
        name = coder.decodeObject(of: NSString.self, forKey: "name")! as String
        rootNote = coder.decodeInteger(forKey: "rootNote")

        super.init()
        audioFile = makeAudioFile()
    }

    public func makeAudioFile() -> AVAudioFile? {
        guard let url = Bundle.main.resourceURL?.appendingPathComponent(file) else { return nil}
        do {
            return try AVAudioFile(forReading: url)
        } catch {
            Log("Could not load: \(String(describing: name))")
            return nil
        }
    }


    init(prettyName: String, file: String, note: Int) {
        name = prettyName
        rootNote = note
        id = UUID()
        self.file = file
        super.init()
        audioFile = makeAudioFile()
    }

}
