# SampleMangler

iOS Programming final project. Allows for four samples to be "mangled" and effected using iPhone tilt controls.

This code is still quite early in production and has multiple quality of life improvements and features left to be implemented. This is also serving as my Swift practice, meaning it does not use all of the best practices of the language.
